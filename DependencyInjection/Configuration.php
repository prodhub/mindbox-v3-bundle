<?php

namespace ADW\MindboxV3Bundle\DependencyInjection;

use ADW\MindboxV3Bundle\Entity\Customer;
use ADW\MindboxV3Bundle\Operation\Customer\CreateCustomerOperation;
use ADW\MindboxV3Bundle\Operation\Customer\CustomerPasswordAuthOperation;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext;
use ADW\MindboxV3Bundle\Operation\Customer\LoadCustomerOperation;
use ADW\MindboxV3Bundle\Operation\Customer\SaveCustomerOperation;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_mindbox_v3');

        $rootNode
            ->children()
                ->arrayNode('client')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset(false)
                    ->children()
                        ->scalarNode('cache_folder')
                            ->defaultValue('proxy_cache')
                        ->end()
                        ->scalarNode('cookie_device_uuid')
                            ->defaultValue('mindboxDeviceUUID')
                        ->end()
                        ->scalarNode('fetch_all_on_destruct')
                            ->defaultFalse()
                        ->end()
                        ->scalarNode('has_custom_security_handler')
                            ->defaultFalse()
                        ->end()
                        ->scalarNode('default_user_provider')
                            ->defaultFalse()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('classes')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset(false)
                        ->children()
                            ->scalarNode('customer_class')
                                ->defaultValue(Customer::class)
                            ->end()
                            ->scalarNode('load_customer_operation_class')
                                ->defaultValue(LoadCustomerOperation::class)
                            ->end()
                            ->scalarNode('save_customer_operation_class')
                                ->defaultValue(SaveCustomerOperation::class)
                            ->end()
                            ->scalarNode('create_customer_operation_class')
                                ->defaultValue(CreateCustomerOperation::class)
                            ->end()
                            ->scalarNode('auth_operation_class')
                                ->defaultValue(CustomerPasswordAuthOperation::class)
                            ->end()
                            ->scalarNode('context_class')
                                ->defaultValue(CustomerOperationContext::class)
                            ->end()
                        ->end()
                ->end()
                ->arrayNode('credentials')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset(false)
                    ->children()
                        ->scalarNode('secret_key')
                            ->isRequired()
                        ->end()
                        ->scalarNode('end_point')
                            ->isRequired()
                        ->end()
                        ->booleanNode('telegram_notification_enabled')
                            ->defaultFalse()
                        ->end()
                    ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
