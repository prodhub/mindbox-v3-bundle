<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 21.03.18
 * Time: 13:18
 */

namespace ADW\MindboxV3Bundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

class MindboxFactory implements SecurityFactoryInterface
{
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.mindbox';
        $listenerId = sprintf('security.authentication.listener.mindbox.%s', $id);

        $container->setDefinition($listenerId, new DefinitionDecorator('security.authentication.listener.mindbox'))
                  ->addMethodCall('setSingleIp', [$config['one_session_one_ip']]);

        return [$providerId, $listenerId, $defaultEntryPoint];
    }

    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'mindbox';
    }

    public function addConfiguration(NodeDefinition $node)
    {
        $node
            ->children()
                ->booleanNode('one_session_one_ip')->defaultFalse()
            ->end();
    }
}