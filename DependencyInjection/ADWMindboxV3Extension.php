<?php

namespace ADW\MindboxV3Bundle\DependencyInjection;

use ADW\MindboxV3Bundle\Entity\CustomerInterface;
use ADW\MindboxV3Bundle\EventListener\QueueCloserListener;
use ADW\MindboxV3Bundle\Helper\RequestId;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext;
use ADW\MindboxV3Bundle\Operation\Lib\OperationInterface;
use ADW\MindboxV3Bundle\Service\MindboxQueueService;
use Monolog\Handler\RotatingFileHandler;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ADWMindboxV3Extension extends Extension
{
    const MONOLOG_CHANNEL = 'adw.mindbox';
    const F_PROCESSOR     = 'adw.request_id';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->moveConfiguration(
            $this->processConfiguration(new Configuration(), $configs),
            $container
        );

        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.customer_class'),
            CustomerInterface::class
        );
        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.load_customer_operation_class'),
            OperationInterface::class
        );
        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.save_customer_operation_class'),
            OperationInterface::class
        );
        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.create_customer_operation_class'),
            OperationInterface::class
        );
        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.auth_operation_class'),
            OperationInterface::class
        );
        $this->isInstanceOf(
            $container->getParameter('adw_mindbox.classes.context_class'),
            CustomerOperationContext::class
        );

        if (!$container->hasDefinition($adwProcessor = self::F_PROCESSOR)) {
            $def = new Definition(RequestId::class);
            $def->setPublic(true);
            $container->setDefinition($adwProcessor, $def);
        }

        $this->createLogger(
            sprintf('monolog.logger.%s', self::MONOLOG_CHANNEL),
            ['path' => $container->getParameter('kernel.logs_dir') . '/' . self::MONOLOG_CHANNEL . '-' . $container->getParameter('kernel.environment') . '.log',],
            $container
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    protected function createLogger($loggerId, $config, ContainerBuilder $container)
    {
        $config = $this->prepareLoggerConfig($config);

        if (!$container->hasDefinition($processor1Id = 'monolog.processor.psr_log_message')) {
            $processor = new Definition('Monolog\\Processor\\PsrLogMessageProcessor');
            $processor->setPublic(false);
            $container->setDefinition($processor1Id, $processor);
        }

        $handler = new Definition(RotatingFileHandler::class);
        $handler->addMethodCall('pushProcessor', [[new Reference(self::F_PROCESSOR), 'processRecord']]);
        $handler->addMethodCall('pushProcessor', [new Reference($processor1Id)]);
        $handler->setArguments([
            $config['path'],
            $config['max_files'],
            $config['level'],
            $config['bubble'],
            $config['file_permission'],
        ]);
        $handler->addMethodCall('setFilenameFormat', [
            $config['filename_format'],
            $config['date_format'],
        ]);

        $container->setDefinition($handlerId = sprintf('%s.handler', $loggerId), $handler);

        $logger = new DefinitionDecorator('monolog.logger_prototype');
        $logger->addMethodCall('pushHandler', [new Reference($handlerId)]);

        $container->setDefinition($loggerId, $logger);
    }

    private function prepareLoggerConfig(array $config = [])
    {
        return $config + [
                'type'            => 'rotating_file',
                'path'            => '',
                'max_files'       => 0,
                'level'           => Logger::DEBUG,
                'filename_format' => '{filename}-{date}',
                'date_format'     => 'Y-m-d',
                'file_permission' => null,
                'bubble'          => true,
            ];
    }

    private function moveConfiguration($config, ContainerBuilder $container, $parent = 'adw_mindbox')
    {
        $config = (array)$config;
        if(!$config || empty($config)) {
            return;
        }

        foreach($config as $item => $value) {
            $key = sprintf('%s.%s', $parent, $item);
            if(\is_array($value)) {
                $this->moveConfiguration($value, $container, $key);
            } else {
                $container->setParameter($key, $value);
            }
        }
    }

    private function isInstanceOf($className, $instanceOf)
    {
        if(trim($className, '\\') !== $instanceOf) {
            $class = null;
            if(\class_exists($className)) {
                $class = new $className;
            }
            if(!$class || !is_subclass_of($class, $instanceOf)) {
                throw new \InvalidArgumentException(sprintf('Class `%s` must be instance of `%s`', $className, $instanceOf));
            }
        }
    }
}
