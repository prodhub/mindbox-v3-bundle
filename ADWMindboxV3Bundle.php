<?php

namespace ADW\MindboxV3Bundle;

use ADW\MindboxV3Bundle\DependencyInjection\Security\Factory\MindboxFactory;
use Symfony\Bundle\FrameworkBundle\DependencyInjection\FrameworkExtension;
use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ADWMindboxV3Bundle extends Bundle
{
    const E_BAD_CREDENTIALS = 'Неверный логин/пароль';

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        /** @var SecurityExtension $security */
        $security = $container->getExtension('security');
        $security->addSecurityListenerFactory(new MindboxFactory());
    }
}