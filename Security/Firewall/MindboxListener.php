<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 21.03.18
 * Time: 12:58
 */

namespace ADW\MindboxV3Bundle\Security\Firewall;

use ADW\MindboxV3Bundle\Security\Token\ExternalCustomerToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class MindboxListener implements ListenerInterface
{
    protected $tokenStorage;
    protected $authenticationManager;
    protected $isSingleIp;

    const DEFAULT_TYPE = ExternalCustomerToken::AUTH_TYPE_EMAIL;
    const F_TYPE       = '_auth_type';
    const F_USERNAME   = '_username';
    const F_PASSWORD   = '_password';

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage          = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    public function handle(GetResponseEvent $event)
    {
        $request        = $event->getRequest();
        $authType       = $request->request->get(self::F_TYPE, self::DEFAULT_TYPE);
        $username       = $request->request->get(self::F_USERNAME);
        $password       = $request->request->get(self::F_PASSWORD);
        $token          = $this->tokenStorage->getToken();
        $hasCredentials = $authType && $username && $password;
        $checksum       = $this->checksum($event);

        if($hasCredentials) {
            $token = new ExternalCustomerToken([
                ExternalCustomerToken::F_AUTH_TYPE => $authType,
                ExternalCustomerToken::F_USERNAME  => $username,
                ExternalCustomerToken::F_PASSWORD  => $password,],
                $checksum
            );
        }

        if(!$token instanceof ExternalCustomerToken) {
            return;
        }

        if($token->isAuthenticated() && !$token->isChecksum($checksum)) {
            //XSS
            $this->tokenStorage->setToken(null);
            $response = new RedirectResponse('/');
            $event->setResponse($response);
            return;
        }

        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
            return;
        } catch(AuthenticationException $failed) {
            $this->tokenStorage->setToken(null);
        }

        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }

    public function setSingleIp($bool)
    {
        $this->isSingleIp = (bool)$bool;
    }

    private function checksum(GetResponseEvent $event)
    {
        $request   = $event->getRequest();
        $userAgent = $request->headers->get('User-Agent');
        $suffix    = null;
        if(!\is_string($userAgent)) {
            $userAgent = json_encode($userAgent);
        }
        if($this->isSingleIp) {
            $suffix = (string)$request->getClientIp();
        }
        return sha1($userAgent . $suffix);
    }
}