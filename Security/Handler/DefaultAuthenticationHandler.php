<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 02.04.18
 * Time: 17:28
 */

namespace ADW\MindboxV3Bundle\Security\Handler;

use ADW\MindboxV3Bundle\ADWMindboxV3Bundle;
use ADW\MindboxV3Bundle\Entity\Customer;
use ADW\MindboxV3Bundle\Security\Token\ExternalCustomerToken;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DefaultAuthenticationHandler implements EventSubscriberInterface
{
    const PREFIX = 'mindbox_id_%d';
    /** @var EntityManagerInterface */
    protected $em;
    /** @var UserProviderInterface */
    protected $userProvider;
    protected $isDisabled;
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents() :array
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN            => 'onAuthenticationSuccess',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
        ];
    }

    public function onAuthenticationSuccess(AuthenticationEvent $event)
    {
        if($this->isDisabled || !method_exists($this->userProvider, 'loadUserByUsername')) {
            return;
        }

        $token = $event->getAuthenticationToken();
        if($token instanceof ExternalCustomerToken && $token->isAuthenticated()) {
            $customer = $token->getCustomer();
            if(!$customer instanceof Customer) {
                return;
            }
            $username = sprintf(self::PREFIX, $token->getId());
            try {
                $user = $this->userProvider->loadUserByUsername($username);
            } catch(UsernameNotFoundException $e) {
                $user = null;
            }
            if(null === $user) {
                //@todo: write custom fields for new users, if need
                $user = new User();
                $user->setUsername($username);
                $user->setEmail($customer->getEmail());
                $user->setPlainPassword(sha1('your_never_unlock_it_' . random_bytes(255)));
                $this->em->persist($user);
                $this->em->flush();
            }
            $token->setUser($user);
        }
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        if($this->isDisabled) {
            return;
        }

        $token = $event->getAuthenticationToken();
        if($token instanceof ExternalCustomerToken && !$token->isAuthenticated()) {
            throw new BadRequestHttpException(ADWMindboxV3Bundle::E_BAD_CREDENTIALS);
        }
    }

    public function setEm(Registry $registry)
    {
        $this->em = $registry->getManager();
    }

    public function setUserProvider($userProviderAlias)
    {
        if($this->container && $this->container->has($userProviderAlias)) {
            $this->userProvider = $this->container->get($userProviderAlias);
        }
    }

    public function setIsDisabled($bool)
    {
        $this->isDisabled = (bool)$bool;
    }
}