<?php

namespace ADW\MindboxV3Bundle\Security\Provider;


use ADW\MindboxV3Bundle\Event\AuthorisedCustomerEvent;
use ADW\MindboxV3Bundle\Repository\CustomerRepository;
use ADW\MindboxV3Bundle\Security\Token\CustomerTokenInterface;
use ADW\MindboxV3Bundle\Security\Token\ExternalCustomerToken;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class MindboxAuthenticationProvider implements AuthenticationProviderInterface
{
    /** @var CustomerRepository */
    protected $customerRepository;

    /**
     * @param CustomerTokenInterface $token
     * @throws \LogicException
     * @throws AuthenticationException
     * @return TokenInterface
     */
    public function authenticate(TokenInterface $token)
    {
        /** @var CustomerTokenInterface $token */
        if(!$this->customerRepository instanceof CustomerRepository) {
            throw new \LogicException(sprintf('Do you forget to call $this->setCustomerRepository() for `%s` in service.yml?', basename(__CLASS__)));
        }
        if(!$token instanceof ExternalCustomerToken) {
            return $token;
        }

        $customer = null;
        $tokenId  = $token->getId();
        if(!$tokenId || !$token->isAuthenticated()) {
            $token->setAuthenticated($this->customerRepository->auth($token));
        }

        if($token->getId() > 0 && !$token->getCustomer()) {
            $customer = $this->customerRepository->find($token->getId());
        }

        if($customer) {
            if(!$token->isAuthenticated()) {
                $token->setAuthenticated($customer);
            }
            $token->setCustomer($customer);
        }

        if(!$token->isAuthenticated()) {
            throw new AuthenticationException('Unable to authenticate in mindbox');
        }

        return $token;
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof CustomerTokenInterface;
    }

    public function setCustomerRepository(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }
}