<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 21.03.18
 * Time: 10:29
 */

namespace ADW\MindboxV3Bundle\Security\Token;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface CustomerTokenInterface extends TokenInterface
{
    const AUTH_TYPE_EMAIL = 'email';
    const AUTH_TYPE_PHONE = 'phone';
    const F_USERNAME      = 'username';
    const F_PASSWORD      = 'password';
    const F_AUTH_TYPE     = 'usernameType';

    public function setCustomer($customer);
    public function getCustomer();
    public function getId();
    public function isChecksum($checksum);
}