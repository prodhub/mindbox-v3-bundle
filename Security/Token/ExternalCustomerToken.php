<?php

namespace ADW\MindboxV3Bundle\Security\Token;

use ADW\MindboxV3Bundle\Entity\CustomerInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ExternalCustomerToken implements CustomerTokenInterface
{
    /**
     * Local profile entity
     * @var UserInterface
     */
    protected $user;
    /**
     * External profile entity
     * @var CustomerInterface
     */
    protected $customer;
    protected $authenticated;
    protected $attributes;
    protected $credentials;
    protected $username;
    protected $checksum;
    /** @var RoleInterface[] */
    private $roles = [];

    public function __construct($credentials, $checksum)
    {
        $this->credentials = $credentials;
        $this->checksum    = $checksum;
    }

    public function getId()
    {
        return $this->username;
    }

    /**
     * @param $checksum
     * @return mixed
     */
    public function isChecksum($checksum)
    {
        return $checksum === $this->checksum;
    }

    /*
     * Internal user
     */
    public function setUser($user)
    {
        if(!$user instanceof UserInterface) {
            throw new \InvalidArgumentException(sprintf('Variable $user must be instance of UserInterface, got `%s`', basename(\get_class($user))));
        }

        $this->setRoles($user->getRoles());
        $this->user = $user;
    }

    /*
     * External user
     */
    public function setCustomer($customer)
    {
        if(!$customer instanceof CustomerInterface) {
            throw new \InvalidArgumentException(sprintf('Variable $customer must be instance of CustomerInterface, got `%s`', basename(\get_class($customer))));
        }

        $this->customer = $customer;
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return CustomerInterface
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    public function isAuthenticated() :bool
    {
        return (bool)$this->authenticated;
    }

    /**
     * @param mixed $object
     */
    public function setAuthenticated($object)
    {
        $id = 0;
        if(\is_object($object) && method_exists($object, 'getId')) {
            $id = $object->getId();
        }
        $this->authenticated = $id > 0;
        $this->authenticated && $this->username = $id;
    }

    public function serialize()
    {
        return serialize(
            [
                $this->checksum,
                $this->username,
                $this->authenticated,
                $this->attributes,
            ]
        );
    }

    public function unserialize($serialized)
    {
        list($this->checksum, $this->username, $this->authenticated, $this->attributes,) = unserialize($serialized, []);
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
        $this->credentials = null;
    }

    public function getCredentials()
    {
        return $this->credentials;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute($name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new \InvalidArgumentException(sprintf('This token has no "%s" attribute.', $name));
        }

        return $this->attributes[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function __toString()
    {
        $roles = [];

        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }

        return sprintf('%s(user="%s", authenticated=%d, roles="%s")', basename(\get_class($this)), $this->getUsername(),
            (int)$this->authenticated, implode(';', $roles));
    }

    private function setRoles(array $roles)
    {
        $this->roles = [];
        foreach($roles as $role) {
            if(\is_string($role)) {
                $role = new Role($role);
            } elseif(!$role instanceof RoleInterface) {
                throw new \InvalidArgumentException(
                    sprintf('$roles must be an array of strings, or RoleInterface instances, but got %s.', \gettype($role)));
            }

            $this->roles[] = $role;
        }
    }
}