<?php

namespace ADW\MindboxV3Bundle\Service;

use ADW\MindboxV3Bundle\Accessor\Lib\AccessorInterface;
use ADW\MindboxV3Bundle\Accessor\Lib\ProxyAccessorInterface;
use ADW\MindboxV3Bundle\Client\ClientInterface;
use ADW\MindboxV3Bundle\Operation\Lib\OperationAbstract;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContext;
use ADW\MindboxV3Bundle\Operation\Lib\OperationInterface;
use ADW\MindboxV3Bundle\Security\Token\CustomerTokenInterface;
use ADW\MindboxV3Bundle\Service\Lib\OperationQueueInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class MindboxQueueService implements OperationQueueInterface
{
    const STATUS_FETCHED   = 'fetched';
    const STATUS_IDLE      = 'idle';
    const STATUS_SENT      = 'sent';
    const DEFAULT_STATUS   = self::STATUS_IDLE;
    const IS_ALREADY_SEND_STATUSES = [
        self::STATUS_SENT    => true,
        self::STATUS_FETCHED => true,
    ];

    /** @var OperationInterface[] */
    protected $queue  = [];
    protected $status = [];
    /** @var array */
    protected $env = [];
    protected $isQueueFetched = false;

    /**
     * Once sync run for operation with unset queue and status history
     * @param OperationInterface $operation
     * @param AccessorInterface $accessor
     */
    public function run_operation(OperationInterface $operation, AccessorInterface $accessor = null)
    {
        $alias = $this->send_operation($operation);
        $isProxyRequest = false;

        if($accessor) {
            $accessor->setContainer($operation->getContainer());
            if($accessor instanceof ProxyAccessorInterface) {
                $isProxyRequest = true;
                $accessor->setFetchFn(function () use ($alias) {
                    $this->fetch_queue_element($alias);
                });
            }
        }
        if(!$isProxyRequest) {
            $this->fetch_queue_element($alias);
        }
    }

    /**
     * Push operation at queue for delayed call
     * @param OperationInterface $operation
     * @param string|int $alias
     * @see: https://medium.com/@rtheunissen/efficient-data-structures-for-php-7-9dda7af674cd
     * @return int|string
     */
    public function push_queue_element($operation, $alias = null)
    {
        if(!$alias) {
            $alias = \spl_object_hash($operation);
        }

        $this->status[$alias] = self::STATUS_IDLE;
        $this->queue[$alias]  = $operation;

        return $alias;
    }

    /**
     * Send request for one queue element
     * @param mixed $alias
     */
    public function send_queue_element($alias)
    {
        if(!isset($this->status[$alias], self::IS_ALREADY_SEND_STATUSES[$this->status[$alias]])) {
            $this->configure($alias);
            $this->status[$alias] = self::STATUS_SENT;
        }
    }

    /**
     * Read answer for one queue element
     * @param mixed $alias
     */
    public function fetch_queue_element($alias)
    {
        if(!isset($this->status[$alias]) || $this->status[$alias] !== self::STATUS_FETCHED) {
            $this->run($alias);
            $this->status[$alias] = self::STATUS_FETCHED;
        }
    }

    /**
     * Send operation and push to queue container, return alias of queue element.
     * Next operation: $this->fetch_queue_element($alias);
     * @param OperationInterface $operation
     * @param mixed $alias
     * @return mixed
     */
    public function send_operation(OperationInterface $operation, $alias = null)
    {
        $this->send_queue_element($alias = $this->push_queue_element($operation, $alias));
        return $alias;
    }


    /**
     * Pull operation from queue
     * @param int $alias
     * @param $default
     * @return OperationInterface|null
     */
    public function pull_queue_element($alias, $default = null) :OperationInterface
    {
        if(!isset($this->queue[$alias], $this->status[$alias])) {
            return $default;
        }

        return $this->queue[$alias];
    }

    /**
     * Send all queue
     */
    public function send_queue()
    {
        if($this->status) {
            foreach($this->status as $alias => $status) {
                $this->send_queue_element($alias);
            }
        }
    }

    /**
     * Read all queue
     */
    public function fetch_queue()
    {
        if(!empty($this->status)) {
            foreach($this->status as $alias => $status) {
                $this->fetch_queue_element($alias);
            }
        }
    }

    public function run_queue()
    {
        $this->send_queue();
        $this->fetch_queue();
    }

    /**
     * Extract operation with unset from a queue storage
     * @param $alias
     * @param null $default
     * @return OperationInterface
     */
    public function extract_queue_element($alias, $default = null) :OperationInterface
    {
        if($default === $operation = $this->pull_queue_element($alias, $default)) {
            return $default;
        }

        $operation = $this->queue[$alias];
        unset($this->queue[$alias], $this->status[$alias]);

        return $operation;
    }

    public function setClient(ClientInterface $client)
    {
        $this->env[OperationContext::E_CLIENT] = $client;
    }

    public function setClientAsync(ClientInterface $client)
    {
        $this->env[OperationContext::E_CLIENT_ASYNC] = $client;
    }

    public function setSecret($secret)
    {
        $this->env[OperationContext::E_SECRET] = $secret;
    }

    public function setEndPoint($endPoint)
    {
        $this->env[OperationContext::E_ENDPOINT_ID] = $endPoint;
    }

    public function setCustomerVars(RequestStack $request, $cookieName, TokenStorageInterface $tokenStorage = null)
    {
        if($r = $request->getMasterRequest()) {
            $this->env[OperationContext::E_CLIENT_IP]   = $r->getClientIp();
            $this->env[OperationContext::E_DEVICE_GUID] = $r->cookies->get($cookieName);
        }

        $this->env[OperationContext::E_FN_USER_ID] = function () use ($tokenStorage) {
            $userId = null;
            if($tokenStorage && $tokenStorage instanceof TokenStorageInterface && $token = $tokenStorage->getToken()) {
                if($token instanceof CustomerTokenInterface) {
                    $userId = $token->getId();
                } else if ($token instanceof TokenInterface && \is_object($user = $token->getUser()) && method_exists($user, 'getId')) {
                    $userId = $user->getId();
                }
            }
            return $userId;
        };
    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->env[OperationContext::E_EVENT_DISPATCHER] = $dispatcher;
    }

    /**
     * @param string|int $alias
     */
    protected function configure($alias)
    {
        if(false !== $operation = $this->pull_queue_element($alias, false)) {
            $this->follow($operation);
            $operation->tune();
            $operation->prepare();
        }
    }

    /**
     * @param mixed $alias
     */
    protected function run($alias)
    {
        if(false !== $operation = $this->pull_queue_element($alias, false)) {
            $operation->run();
        }
    }

    public function close()
    {
        if(!$this->isQueueFetched) {
            $this->fetch_queue();
            $this->isQueueFetched = true;
        }
    }

    /**
     * Set environment setting in context (mindbox brand, staff credentials)
     * @param $operation
     */
    protected function follow($operation)
    {
        if(!$operation instanceof OperationAbstract) {
            return;
        }

        if(!$this->env) {
            return;
        }

        $context = $operation->getContext();
        foreach($this->env as $key => $value) {
            $context->setEnv($key, $value);
        }
    }
}