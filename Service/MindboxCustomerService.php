<?php

namespace ADW\MindboxV3Bundle\Service;

use ADW\MindboxV3Bundle\Entity\CustomerInterface;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext as Context;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContainer as Container;
use ADW\MindboxV3Bundle\Security\Token\CustomerTokenInterface;
use ADW\MindboxV3Bundle\Service\Lib\OperationQueueInterface;

class MindboxCustomerService
{
    /** @var OperationQueueInterface */
    protected $queue;
    protected $queueIds = [];
    protected $_contextClass;
    protected $_loadCustomerOperationClass;
    protected $_saveCustomerOperationClass;
    protected $_createCustomerOperationClass;
    protected $_authOperationClass;

    public function auth($className, CustomerTokenInterface $token): CustomerInterface
    {
        /** @var Context $context */
        $context = new $this->_contextClass;
        $context->fill([
            $context::F_TOKEN_OBJECT => $token,
        ]);
        /** @var CustomerInterface $customer */
        $this->queue->run_operation(new $this->_authOperationClass(
            $context
        ), $customer = new $className);

        return $customer;
    }

    /**
     * @param string $className
     * @param array $criteria
     * @return CustomerInterface
     */
    public function find($className, array $criteria): CustomerInterface
    {
        /** @var CustomerInterface $customer */
        $this->queue->run_operation(new $this->_loadCustomerOperationClass(
            new $this->_contextClass($criteria)
        ), $entity = new $className);

        return $entity;
    }

    public function merge($object)
    {
        /** @var Context $context */
        $context = new $this->_contextClass;
        $context->fill([
            $context::F_CUSTOMER_OBJECT => $object,
        ]);

        $this->queueIds[] = $this->queue->send_operation(
            new $this->_saveCustomerOperationClass(
                $context
            )
        );
    }

    public function persist($object)
    {
        /** @var Context $context */
        $context = new $this->_contextClass;
        $context->fill([
            $context::F_CUSTOMER_OBJECT => $object,
        ]);

        $this->queueIds[] = $this->queue->send_operation(
            new $this->_createCustomerOperationClass(
                $context
            )
        );
    }

    public function flush()
    {
        if($this->queueIds) {
            foreach($this->queueIds as $queueId) {
                $this->queue->fetch_queue_element($queueId);
                $this->queue->extract_queue_element($queueId);
            }
            $this->queueIds = null;
        }
    }

    public function setQueue(OperationQueueInterface $queue)
    {
        $this->queue = $queue;
    }

    public function setContextClass($className)
    {
        $this->_contextClass = $className;
    }

    public function setSaveOperationClass($className)
    {
        $this->_saveCustomerOperationClass = $className;
    }

    public function setCreateOperationClass($className)
    {
        $this->_createCustomerOperationClass = $className;
    }

    public function setLoadOperationClass($className)
    {
        $this->_loadCustomerOperationClass = $className;
    }

    public function setAuthOperationClass($className)
    {
        $this->_authOperationClass = $className;
    }
}