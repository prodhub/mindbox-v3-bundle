<?php

namespace ADW\MindboxV3Bundle\Service\Lib;

interface QueueInterface
{
    public function push_queue_element($object, $alias = null);
    public function pull_queue_element($alias, $default = null);
    public function extract_queue_element($alias, $default = null);
    public function run_queue();
}