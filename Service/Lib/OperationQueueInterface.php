<?php

namespace ADW\MindboxV3Bundle\Service\Lib;

use ADW\MindboxV3Bundle\Accessor\Lib\AccessorInterface;
use ADW\MindboxV3Bundle\Operation\Lib\OperationInterface;

interface OperationQueueInterface extends AsyncQueueInterface
{
    public function send_operation(OperationInterface $operation, $alias = null);
    public function push_queue_element($operation, $alias = null);
    public function run_operation(OperationInterface $operation, AccessorInterface $accessor = null);
    public function pull_queue_element($alias, $default = null) :OperationInterface;
    public function extract_queue_element($alias, $default = null) :OperationInterface;
}