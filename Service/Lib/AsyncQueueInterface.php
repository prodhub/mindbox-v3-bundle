<?php

namespace ADW\MindboxV3Bundle\Service\Lib;

interface AsyncQueueInterface extends QueueInterface
{
    public function send_queue();
    public function fetch_queue();
    public function send_queue_element($alias);
    public function fetch_queue_element($alias);
}