<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 17:47
 */

namespace ADW\MindboxV3Bundle\Accessor;


use ADW\MindboxV3Bundle\Accessor\Lib\AccessorInterface;

class DataRequest implements AccessorInterface
{
    protected $container;

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }
}