<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 17:37
 */

namespace ADW\MindboxV3Bundle\Accessor;


use ADW\MindboxV3Bundle\Accessor\Lib\ProxyAccessorInterface;

class ProxyDataRequest extends DataRequest implements ProxyAccessorInterface
{
    protected $fn;
    protected $_isFetched;

    public function getFetchedContainer()
    {
        if(!$this->_isFetched) {
            $this->fetch();
        }

        return $this->container;
    }

    public function setFetchFn(\Closure $fn)
    {
        $this->fn = $fn;
    }

    public function fetch()
    {
        if(!$this->_isFetched) {
            \is_callable($this->fn) && ($this->fn)();
            $this->_isFetched = true;
        }

        return $this;
    }
}