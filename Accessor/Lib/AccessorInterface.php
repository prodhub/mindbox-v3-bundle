<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 17:34
 */

namespace ADW\MindboxV3Bundle\Accessor\Lib;


interface AccessorInterface
{
    public function setContainer($container);
    public function getContainer();
}