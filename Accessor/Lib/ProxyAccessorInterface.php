<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 17:36
 */

namespace ADW\MindboxV3Bundle\Accessor\Lib;


interface ProxyAccessorInterface extends AccessorInterface
{
    public function setFetchFn(\Closure $fn);
    public function fetch();
    public function getFetchedContainer();
}