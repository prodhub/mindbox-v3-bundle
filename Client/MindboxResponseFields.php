<?php

namespace ADW\MindboxV3Bundle\Client;


class MindboxResponseFields
{
    const F_STATUS        = 'status';
    const F_CUSTOMER      = 'customer';
    const F_RESULT        = 'result';
    const F_V_MESSAGES    = 'validationMessages';
    const F_MESSAGE       = 'message';
    const F_ERROR_MESSAGE = 'errorMessage';
    const F_ERROR_ID      = 'errorId';
    const F_HTTP_CODE     = 'httpStatusCode';

    const S_V_ERROR = 'ValidationError';
    const S_P_ERROR = 'ProtocolError';
    const S_I_ERROR = 'InternalServerError';
    const S_T_ERROR = 'TransactionAlreadyProcessed';
    const S_SUCCESS = 'Success';
}