<?php

namespace ADW\MindboxV3Bundle\Client;

use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Client\Lib\MindboxResponse;
use Symfony\Component\Process\Process;

/**
 * Fire and forget async client for mindbox requests
 * Class MindboxAsyncClient
 * @package ADW\MindboxV3Bundle\Client
 */
class MindboxAsyncClient implements ClientInterface
{
    private $rootDir;
    private $env;

    public function request(MindboxRequest $context, MindboxResponse $container = null)
    {
        $commandLine = '/usr/bin/php %s/console adw:mindbox:call -e=%s "%s"';

        $process = new Process(sprintf($commandLine, $this->rootDir, $this->env, \base64_encode(serialize($context))), $this->rootDir);
        $process->start();
    }

    public function response(MindboxResponse $container) {}

    public function setRootDir($rootDir)
    {
        if(!\file_exists((string)$rootDir)) {
            throw new \LogicException(sprintf('Are you sure that `%s` is a real root dir? Please, make call setRootDir with `kernel.root_dir` in service.yml', $rootDir));
        }

        $this->rootDir = (string)$rootDir;
    }

    public function setEnv($environment)
    {
        $this->env = (string)$environment;
    }
}