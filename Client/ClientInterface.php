<?php

namespace ADW\MindboxV3Bundle\Client;


use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Client\Lib\MindboxResponse;

interface ClientInterface
{
    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_GET  = 'GET';
    const HEADER_CACHE_TTL = 'X-ADW-Cache';

    public function request(MindboxRequest $context, MindboxResponse $container);
    public function response(MindboxResponse $container);
}