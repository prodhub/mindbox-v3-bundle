<?php

namespace ADW\MindboxV3Bundle\Client;

use ADW\MindboxV3Bundle\Client\Cache\CacheProxyClient;
use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Client\Lib\MindboxResponse;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use function GuzzleHttp\Psr7\stream_for;

class MindboxClient implements ClientInterface
{
    const LOGGER_PATTERN                   = '[%s] %s';
    const LOGGER_INFO_START_REQUEST        = '>>> %s %s. %s. %s';
    const LOGGER_INFO_END_REQUEST          = '<<< %s %s. %s';
    const LOGGER_EXCEPTION_MESSAGE_PATTERN = 'Exception on %s %s: %s. %s. %s at %s';
    const LOGGER_CORRUPT_RESPONSE          = 'Try to read response without promise. %s';
    /** @var GuzzleClientInterface */
    protected $transport;
    /** @var LoggerInterface */
    protected $logger;
    protected $userId;

    /**
     * @param MindboxRequest $context
     * @param MindboxResponse|null $container
     */
    public function request(MindboxRequest $context, MindboxResponse $container = null)
    {
        $this->userId = $context->get(MindboxRequest::F_USER_ID);
        $method       = $context->get(MindboxRequest::F_METHOD, MindboxRequest::DEFAULT_METHOD);
        $uri          = $context->get(MindboxRequest::F_URI);
        $headers      = $context->get(MindboxRequest::F_HEADERS, []);
        $body         = null;
        $options      = [];

        if($context->is(MindboxRequest::F_BODY)) {
            $body = stream_for($context->get(MindboxRequest::F_BODY));
        }

        if($context->is(MindboxRequest::F_TIMEOUT)) {
            $options[RequestOptions::TIMEOUT]         = $context->get(MindboxRequest::F_TIMEOUT);
            $options[RequestOptions::CONNECT_TIMEOUT] = $context->get(MindboxRequest::F_TIMEOUT);
        }

        try {
            $client  = $this->getTransport();
            $request = new Request($method, $uri, $headers, $body);
            $promise = $client->sendAsync($request, $options);

            if($container instanceof MindboxResponse) {
                $container->request = $request;
                $container->promise = $promise;
            }

            $this->stdout(Logger::INFO, sprintf(
                self::LOGGER_INFO_START_REQUEST,
                $method,
                $uri,
                '',
                $context
            ));
        } catch(\Throwable $e) {
            $this->stdout(Logger::CRITICAL, sprintf(
                self::LOGGER_EXCEPTION_MESSAGE_PATTERN,
                $method,
                $uri,
                $e->getMessage(),
                $container,
                $context,
                basename($e->getFile()) . ':' . $e->getLine()
            ));
        }
    }

    public function response(MindboxResponse $container)
    {
        $e      = null;
        $method = $uri = '(?)';

        if($container->request instanceof Request) {
            $method = $container->request->getMethod();
            $uri    = $container->request->getUri();
        }

        try {
            if($container->promise instanceof PromiseInterface) {
                $start = microtime(true);
                $container->promise->then(
                    $container->getFn($container::FN_ON_FULFILLED),
                    $container->getFn($container::FN_ON_REJECTED)
                )->wait(false);
                $container->requestTime += microtime(true) - $start;
                $this->stdout(Logger::INFO, sprintf(
                    self::LOGGER_INFO_END_REQUEST,
                    $container->statusCode,
                    $uri,
                    $container
                ));
            } else {
                $this->stdout(Logger::INFO, sprintf(
                    self::LOGGER_CORRUPT_RESPONSE,
                    $container
                ));
            }

        } catch(\Throwable $e) {
            $container->exception = $e;
            $this->stdout(Logger::CRITICAL, sprintf(
                self::LOGGER_EXCEPTION_MESSAGE_PATTERN,
                $method,
                $uri,
                $e->getMessage(),
                $container,
                'Exception',
                basename($e->getFile()) . ':' . $e->getLine()
            ));
        }
    }

    public function setTransport(GuzzleClientInterface $transport)
    {
        $this->transport = $transport;
    }

    public function getTransport() :GuzzleClientInterface
    {
        if(!$this->transport instanceof GuzzleClientInterface) {
            $this->transport = new CacheProxyClient();
        }

        return $this->transport;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function stdout($level, $message)
    {
        if($this->logger instanceof Logger) {
            $this->logger->log($level, sprintf(
                self::LOGGER_PATTERN,
                $this->userId > 0 ? 'userId#' . $this->userId : 'anon',
                $message
            ));
        }
    }
}