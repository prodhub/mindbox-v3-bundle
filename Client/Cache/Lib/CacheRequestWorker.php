<?php

namespace ADW\MindboxV3Bundle\Client\Cache\Lib;

use ADW\MindboxV3Bundle\Client\Cache\Adapter\AdapterInterface;
use function GuzzleHttp\Psr7\parse_request;
use Psr\Http\Message\RequestInterface;
use function GuzzleHttp\Psr7\parse_response;
use function GuzzleHttp\Psr7\str;
use function GuzzleHttp\Psr7\hash;

class CacheRequestWorker
{
    const TYPE_ARRAY    = 'array';
    const TYPE_RESPONSE = 'response';
    const TYPE_REQUEST  = 'request';

    /** @var AdapterInterface */
    private $adapter;

    public function __construct(array $config = [])
    {
        if(isset($config['adapter'])) {
            $this->adapter = $config['adapter'];
        } else {
            throw new \InvalidArgumentException('Please, set `adapter` in CacheRequestWorker::class');
        }
    }

    public function push($key, $value, $valueType = null)
    {
        if(!\is_string($key) && !\is_numeric($key)) {
            $key = $this->key($key);
        }

        if($valueType) {
            $value = $this->value($value, $valueType);
        }

        return $this->adapter->push($key, $value);
    }

    public function pull($key, $type = null)
    {
        if(!\is_string($key) && !\is_numeric($key)) {
            $key = $this->key($key);
        }

        $output = $this->adapter->pull($key);

        if($type) {
            switch($type) {
                case self::TYPE_RESPONSE:
                    $output = parse_response($output);
                    break;
                case self::TYPE_REQUEST:
                    $output = parse_request($output);
                    break;
                default:
                    throw new \LogicException(sprintf('Undefined type %s', $type));
            }
        }

        return $output;
    }

    public function is($key, $ttl)
    {
        if(!\is_string($key) && !\is_numeric($key)) {
            $key = $this->key($key);
        }

        return $this->adapter->is($key, [
            'ttl' => $ttl
        ]);
    }

    public function key($item)
    {
        switch(true) {
            case $item instanceof RequestInterface:
                $str = sprintf(
                    '%s-%s--%s',
                    $item->getMethod(),
                    $item->getUri()->withScheme(''),
                    hash($item->getBody(), 'crc32')
                );
                break;
            default:
                $str = crc32((string)$item);
                break;
        }

        $str = preg_replace('/[\/\?.]{1}/', '-', $str);
        $str = strtolower($str);
        $str = trim($str, '-');
        return $str;
    }

    public function value($item, $type)
    {
        $output = null;
        switch($type) {
            case self::TYPE_REQUEST:
            case self::TYPE_RESPONSE:
                $output = str($item);
                break;
            case self::TYPE_ARRAY:
                $output = json_encode($item);
                break;
            default:
                throw new \LogicException('I can not to work with this item\'s type');
        }

        return $output;
    }
}