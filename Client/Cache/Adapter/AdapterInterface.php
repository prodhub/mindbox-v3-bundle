<?php

namespace ADW\MindboxV3Bundle\Client\Cache\Adapter;

interface AdapterInterface
{
    public function __construct(array $config = []);
    public function push($key, $value);
    public function pull($key);
    public function is($key, array $config = []);
}