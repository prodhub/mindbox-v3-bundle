<?php

namespace ADW\MindboxV3Bundle\Client\Cache\Adapter;


class CacheFilesystemAdapter implements AdapterInterface
{
    public $path;
    protected $ds = DIRECTORY_SEPARATOR;

    public function __construct(array $config = [])
    {
        if(isset($config['path'])) {
            !is_dir($config['path']) && mkdir($config['path'], 0777, true);
            $this->path = $config['path'];
        } else {
            throw new \InvalidArgumentException('Please, set the path for CacheFilesystemAdapter::class');
        }
    }

    public function pull($key)
    {
        $content = null;

        if(!file_exists($this->path . $this->ds . $key)) {
            return $content;
        }

        try {
            $content = file_get_contents($this->path . $this->ds . $key);
        } catch(\Exception $e) {}

        return $content;
    }

    public function push($key, $value)
    {
        file_put_contents($this->path . $this->ds . $key, $value);
    }

    public function is($key, array $config = []) :bool
    {
        if(!file_exists($this->path . $this->ds . $key)) {
            return false;
        }

        if(isset($config['ttl']) && time() - filemtime($this->path . $this->ds . $key) > $config['ttl']) {
            return false;
        }

        return true;
    }
}