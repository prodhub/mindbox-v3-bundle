<?php

namespace ADW\MindboxV3Bundle\Client\Cache;

use ADW\MindboxV3Bundle\Client\Cache\Adapter\CacheFilesystemAdapter;
use ADW\MindboxV3Bundle\Client\Cache\Lib\CacheRequestWorker;
use ADW\MindboxV3Bundle\Client\ClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

class CacheProxyClient extends Client
{
    const CONFIG_CACHE_PATH = 'cachePath';
    /** @var CacheRequestWorker */
    protected $cache;

    public function sendAsync(RequestInterface $request, array $options = [])
    {
        $promise = null;
        $ttl     = $request->getHeader(ClientInterface::HEADER_CACHE_TTL)[0] ?? 0;

        if($ttl > 0 && $this->cache instanceof CacheRequestWorker) {
            $cache = $this->cache;
            $key   = $this->cache->key($request);

            if($cache->is((string)$key, (int)$ttl)) {
                $promise = new FulfilledPromise(
                    $cache->pull($key, CacheRequestWorker::TYPE_RESPONSE)
                );
            }

            if(!$promise) {
                $promise = parent::sendAsync($request, $options);
                $promise->then(function ($response) use ($cache, $key) {
                    if($response instanceof Response) {
                        $cache->push($key, $response, CacheRequestWorker::TYPE_RESPONSE);
                    }
                });
            }
        }

        if(!$promise) {
            $promise = parent::sendAsync($request, $options);
        }

        return $promise;
    }

    public function requestAsync($method, $uri = '', array $options = [])
    {
        throw new \RuntimeException('Please, use sendAsync() or implement proxy for this method');
    }

    public function setCacheEngine(CacheRequestWorker $worker)
    {
        $this->cache = $worker;
    }
}