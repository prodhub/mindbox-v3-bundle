<?php

namespace ADW\MindboxV3Bundle\Client\Lib;

use ADW\MindboxV3Bundle\Helper\ModularContainer;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use function GuzzleHttp\Psr7\rewind_body;

/**
 * Fit and public container for mindbox answers
 *
 * Class MindboxResponse
 * @package ADW\MindboxV3Bundle\Client\Lib
 */
class MindboxResponse extends ModularContainer
{
    const FN_ON_FULFILLED = 'onFulfilled';
    const FN_ON_REJECTED  = 'onRejected';
    /** @var RequestInterface */
    public $request;
    /** @var PromiseInterface */
    public $promise;
    /** @var ResponseInterface */
    public $response;
    /** @var string */
    public $body;
    public $requestTime;
    public $format;
    public $exception;
    public $fn = [];
    public $statusCode;

    public function getFn($type)
    {
        switch($type) {
            case self::FN_ON_FULFILLED:
                if(!isset($this->fn[self::FN_ON_FULFILLED])) {
                    $this->fn[self::FN_ON_FULFILLED] = function($response) {
                        if($response instanceof ResponseInterface) {
                            rewind_body($response);
                            $this->body       = $response->getBody()->getContents();
                            $this->statusCode = (int)$response->getStatusCode();
                            $this->response   = $response;
                        }
                    };
                }
                break;
            case self::FN_ON_REJECTED:
                if(!isset($this->fn[self::FN_ON_REJECTED])) {
                    $this->fn[self::FN_ON_REJECTED] = function ($response) {
                        if($response instanceof RequestException) {
                            $r = $response->getResponse();
                            if($r && $r instanceof ResponseInterface) {
                                $this->statusCode = (int)$r->getStatusCode();
                                rewind_body($r);
                                $this->body     = $r->getBody()->getContents();
                                $this->response = $r;
                            }
                        }
                        if($response instanceof \Exception) {
                            $this->exception = $response;
                        }
                    };
                }
                break;
            default:
                return function($return) {
                    $this->exception = new \RuntimeException(sprintf('Unexpected response function, got: `%s`', serialize($return)));
                };
        }

        return $this->fn[$type];
    }

}