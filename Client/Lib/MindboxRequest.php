<?php

namespace ADW\MindboxV3Bundle\Client\Lib;

use ADW\MindboxV3Bundle\Client\ClientInterface;
use ADW\MindboxV3Bundle\Helper\ModularContext;

class MindboxRequest extends ModularContext
{
    /** <Default values> */
    const DEFAULT_METHOD                  = ClientInterface::HTTP_METHOD_POST;
    const DEFAULT_V3_SYNC_OPERATIONS_URL  = 'https://api.mindbox.ru/v3/operations/sync';
    const DEFAULT_V3_ASYNC_OPERATIONS_URL = 'https://api.mindbox.ru/v3/operations/async';
    /** </Default> */

    /** <Field of request configuration context> */
    const F_METHOD  = 'requestMethod';
    const F_URI     = 'requestURI';
    const F_USER_ID = 'userId';
    const F_HEADERS = 'requestHeaders';
    const F_BODY    = 'requestBody';
    const F_TIMEOUT = 'timeout';
    /** </Field> */
}