<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 03.04.18
 * Time: 16:32
 */

namespace ADW\MindboxV3Bundle\EventListener;


use ADW\MindboxV3Bundle\Service\MindboxQueueService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class QueueCloserListener implements EventSubscriberInterface
{
    protected $queue;
    protected $dispatch;

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::FINISH_REQUEST => 'onKernelFinishRequest',
        ];
    }

    public function onKernelFinishRequest(FinishRequestEvent $event)
    {
        if($this->dispatch) {
            $this->queue instanceof MindboxQueueService && $this->queue->close();
        }
    }

    public function setQueue(MindboxQueueService $queue)
    {
        $this->queue = $queue;
    }

    public function setFetchQueueOnDestruct($bool)
    {
        $this->dispatch = (bool)$bool;
    }
}