<?php

namespace ADW\MindboxV3Bundle\Entity;

use ADW\MindboxV3Bundle\Accessor\Lib\ProxyAccessorInterface;

interface CustomerInterface extends ProxyAccessorInterface
{
    public function getId();
    public function __toString();
}