<?php

namespace ADW\MindboxV3Bundle\Entity;

use ADW\MindboxV3Bundle\Accessor\ProxyDataRequest;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext as Context;

class Customer extends ProxyDataRequest implements CustomerInterface, \ArrayAccess
{
    /*
     * Attention! Call $container via method $this->getFetchedContainer(), because this entity is implement a lazy-load interface
     * @var ModularContainer
     */
    public function getId()
    {
        return $this->getFetchedContainer()->result[Context::F_CUSTOMER][Context::F_IDS][Context::F_ID] ?? null;
    }

    public function getEmail()
    {
        return $this->getFetchedContainer()->result[Context::F_CUSTOMER][Context::F_EMAIL];
    }

    public function offsetExists($offset)
    {
        return method_exists($this, 'get' . ucfirst($offset)) && method_exists($this, 'set' . ucfirst($offset));
    }

    public function offsetGet($offset)
    {
        $method = 'get' . ucfirst($offset);
        return $this->$method();
    }

    public function offsetSet($offset, $value)
    {
        $method = 'set' . ucfirst($offset);
        return $this->$method($value);
    }

    public function offsetUnset($offset) {}

    public function __toString()
    {
        return (string)$this->getFetchedContainer();
    }
}