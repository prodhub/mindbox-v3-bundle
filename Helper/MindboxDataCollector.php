<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 15:39
 */

namespace ADW\MindboxV3Bundle\Helper;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class MindboxDataCollector extends DataCollector
{
    const MINDBOX_COLLECTOR_ID = 'mindbox_v3_collector';
    public $statsHandler;

    public function __construct(GuzzleStats $handler)
    {
        $this->statsHandler = $handler;
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data['calls'] = [];
        foreach($this->statsHandler->stats as $stats) {
            $call['request_body'] = (string)$stats->getRequest()->getBody();
            $call['request']      = $stats->getRequest();
            $call['duration']     = $stats->getTransferTime();

            $h = $stats->hasResponse();
            if($r = $stats->getResponse()) {
                $call['response_body'] = (string)$r->getBody();
                $call['response']      = $r;
                $call['code']          = $r->getStatusCode();
            }

            $this->data['calls'][] = $call;
        }
    }

    public function getCallsCount()
    {
        return !empty($this->data['calls']) ? \count($this->data['calls']) : 0;
    }

    /**
     * @return int
     */
    public function getTotalTime()
    {
        $time = 0;
        if (!empty($calls = &$this->data['calls'])) {
            array_map(function(&$array) use (&$time){
                isset($array['duration']) && $time += $array['duration'];
            }, $calls);
        }

        return $time;
    }

    public function getCountErrors()
    {
        $errors = 0;
        if (!empty($calls = &$this->data['calls'])) {
            array_map(function(&$array) use (&$errors){
                isset($array['code']) && $array['code'] >=500 && ++$errors;
            }, $calls);
        }

        return $errors;
    }

    public function &getCalls()
    {
        return $this->data['calls'];
    }

    public function getName()
    {
        return self::MINDBOX_COLLECTOR_ID;
    }
}