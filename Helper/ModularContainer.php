<?php

namespace ADW\MindboxV3Bundle\Helper;

use function GuzzleHttp\Psr7\str;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class ModularContainer
{
    const TO_STRING_PATTERN = '`%s` => `%s`';
    /**
     * Result of request
     * @var array
     */
    public $result;
    public $status;

    public function __toString()
    {
        $parts = [];
        $vars  = get_object_vars($this);

        foreach($vars as $var => $value) {
            switch(true) {
                case $value instanceof RequestInterface:
                case $value instanceof ResponseInterface:
                    $value = str_replace(["\r", "\n"], ['', '\n'], str($value));
                    break;
                case $value instanceof \Exception:
                    $value = sprintf('%s at %s:%d', $value->getMessage(), basename($value->getFile()), $value->getLine());
                    break;
                case !\is_string($value):
                    $value = json_encode($value);
                    break;
            }
            $parts[] = sprintf(self::TO_STRING_PATTERN, $var, $value);
        }

        return 'ResponseContainer: ' . implode('; ', $parts);
    }
}