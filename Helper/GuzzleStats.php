<?php
/**
 * Author: Anton Lezhepekov <antonlezhepekov@mail.ru>
 * Date: 06.04.18
 * Time: 16:27
 */

namespace ADW\MindboxV3Bundle\Helper;


use GuzzleHttp\TransferStats;

class GuzzleStats
{
    /**
     * @var TransferStats[]
     */
    public $stats = [];
    public function __invoke(TransferStats $stats)
    {
        $this->stats[] = $stats;
    }
}