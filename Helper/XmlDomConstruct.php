<?php

namespace ADW\MindboxV3Bundle\Helper;


class XmlDomConstruct extends \DOMDocument
{
    const ATTR = '@attributes';

    /**
     * Constructs elements and texts from an array or string.
     * The array can contain an element's name in the index part
     * and an element's text in the value part.
     *
     * It can also creates an xml with the same element tagName on the same
     * level.
     *
     * ex:
     * <credentials mode="permanent">
     *   <credential>123</credential>
     *   <password>456</password>
     * </credentials>
     *
     * Array should then look like:
     *
     * $doc   = new XmlDomConstruct('1.0', 'utf-8');
     * $array = [
     *      'credentials' => [
     *          XmlDomConstruct::OPT_ATTRIBUTES => ['mode' => 'permanent'],
     *          'credential'                    => '123',
     *          'password'                      => '456',
     *      ],
     * ];
     * $doc->fromMixed($array);
     *
     * @param mixed $mixed An array or string.
     *
     * @param \DOMElement|XmlDomConstruct $domElement Then element
     * from where the array will be construct to.
     *
     */
    public function fromMixed($mixed, \DOMElement $domElement = null)
    {

        $domElement = $domElement ?? $this;

        if(\is_array($mixed)) {
            foreach($mixed as $index => $mixedElement) {

                if(\is_int($index)) {
                    if($index === 0) {
                        $node = $domElement;
                    } else {
                        $node = $this->createElement($domElement->tagName);
                        $domElement->parentNode->appendChild($node);
                    }
                } else {
                    $node = $this->createElement($index);
                    $domElement->appendChild($node);
                }

                if(isset($mixedElement[self::ATTR])) {
                    if(!empty($mixedElement[self::ATTR])) {
                        foreach((array)$mixedElement[self::ATTR] as $attribute => $value) {
                            $node->setAttribute($attribute, $value);
                        }
                    }
                    unset($mixedElement[self::ATTR]);
                }

                $this->fromMixed($mixedElement, $node);
            }
        } else {
            $domElement->appendChild($this->createTextNode($mixed));
        }

    }
}