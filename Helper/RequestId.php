<?php

namespace ADW\MindboxV3Bundle\Helper;

class RequestId
{
    public static $i = 0;
    public static $requestId;
    public function __construct($id = null)
    {
        self::$requestId = (string)($id ?? self::generate_new_chain());
    }

    private function __clone() {}

    public static function generate_new_chain()
    {
        return trim(chunk_split(hash('md5', random_bytes(32)), 8, '-'), '-');
    }

    public function __invoke()
    {
        return self::$requestId;
    }

    public function processRecord(array $record) :array
    {
        $record['extra']['request_id'] = $this();
        return $record;
    }

    public function __toString()
    {
        return (string)$this();
    }
}