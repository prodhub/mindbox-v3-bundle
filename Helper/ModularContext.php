<?php

namespace ADW\MindboxV3Bundle\Helper;


abstract class ModularContext
{
    const TO_STRING_PATTERN = '`%s` => `%s`';
    /** @var array */
    protected $data = [];
    protected $env;
    /** @var int */
    private $version = 0;

    public function __construct(array $array = [])
    {
        if(!empty($array)) {
            $this->fill($array);
        }
    }

    public function is($key) :bool
    {
        return isset($this->data[$key]) && $this->data[$key];
    }

    public function has($key) :bool
    {
        return isset($this->data[$key]);
    }

    public function fill($array)
    {
        if($this->data) {
            throw new \LogicException('Try to fill already filled context');
        }

        $this->data = $array;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
        $this->version++;
    }

    public function get($key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }

    public function &getLink($key, $default = null)
    {
        if(!isset($this->data[$key])) {
            $this->data[$key] = $default;
        }

        return $this->data[$key];
    }

    public function setEnv($key, $value)
    {
        $this->env[$key] = $value;
    }

    public function getEnv($key, $default = null)
    {
        return $this->env[$key] ?? $default;
    }

    public function &getEnvLink($key, $default = null)
    {
        if(!isset($this->env[$key])) {
            $this->env[$key] = $default;
        }

        return $this->env[$key];
    }

    public function version() :int
    {
        return $this->version;
    }

    public function __toString()
    {
        $string = 'RequestContext (v.' . $this->version() . '): ';
        $parts  = [];

        foreach($this->data as $key => $value) {
            $parts[] = sprintf(
                self::TO_STRING_PATTERN,
                $key,
                json_encode(\is_object($value) ? (array)$value : $value)
            );
        }

        return $string . implode('; ', $parts);
    }
}