<?php

namespace ADW\MindboxV3Bundle\Operation\Customer\Lib;

use ADW\MindboxV3Bundle\Operation\Lib\OperationContext;

class CustomerOperationContext extends OperationContext
{
    const F_EMAIL     = 'email';
    const F_PHONE     = 'mobilePhone';
    const F_PASSWORD  = 'password';
    const F_IDS       = 'ids';
    const F_ID        = 'mindboxId';
    const F_FB        = 'facebook';
    const F_INST      = 'instagram';
    const F_VK        = 'vkontakte';
    const F_CARDS     = 'discountCard';
    const F_OPERATION = 'operation';
    const F_CUSTOMER  = 'customer';

    const F_CUSTOMER_OBJECT = 'customerObject';
    const F_TOKEN_OBJECT    = 'tokenObject';
}