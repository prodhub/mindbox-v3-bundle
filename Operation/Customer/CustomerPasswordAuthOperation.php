<?php

namespace ADW\MindboxV3Bundle\Operation\Customer;

use ADW\MindboxV3Bundle\Client\ClientInterface;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext as Fields;
use ADW\MindboxV3Bundle\Operation\Lib\ConfigBag as Config;
use ADW\MindboxV3Bundle\Operation\Lib\OperationAbstract;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContext as Context;
use ADW\MindboxV3Bundle\Security\Token\CustomerTokenInterface;

/**
 * @see https://danone.directcrm.ru/operations/CustomerPasswordAuthorization/help
 */
class CustomerPasswordAuthOperation extends OperationAbstract
{
    const OPERATION = 'CustomerPasswordAuthorization';

    public function configure(Config $config, Context $context)
    {
        /** Example of simple request in mindbox */
        /** 0) Put the operation's name in a constant */
        $config->set(Config::F_OPERATION, static::OPERATION);
        /** 1) Set method, or use default */
        $config->set(Config::F_METHOD, ClientInterface::HTTP_METHOD_POST);
        /** 2) Set body for POST data. */
        $config->set(Config::F_BODY, $this->body($context));
        /** 3) *Optional. Set custom parameters. See F_* constants in Config::class */
        $config->set(Config::F_HEADERS, []);
        $config->set(Config::F_IS_NEED_AUTH, false);
    }

    private function body(Context $context): array
    {
        $body  = [];
        $token = $context->get(Fields::F_TOKEN_OBJECT);

        if($token instanceof CustomerTokenInterface && $credentials = $token->getCredentials()) {
            $username = Fields::F_EMAIL;
            switch($credentials[CustomerTokenInterface::F_AUTH_TYPE]) {
                case CustomerTokenInterface::AUTH_TYPE_EMAIL:
                    $username = Fields::F_EMAIL;
                    break;
                case CustomerTokenInterface::AUTH_TYPE_PHONE:
                    $username = Fields::F_PHONE;
                    break;
            }
            $body[$username]          = $credentials[CustomerTokenInterface::F_USERNAME];
            $body[Fields::F_PASSWORD] = $credentials[CustomerTokenInterface::F_PASSWORD];
        }

        if($context->is(Fields::F_EMAIL)) {
            $body[Fields::F_EMAIL] = $context->get(Fields::F_EMAIL);
        }
        if($context->is(Fields::F_PHONE)) {
            $body[Fields::F_PHONE] = $context->get(Fields::F_PHONE);
        }
        if($context->is(Fields::F_PASSWORD)) {
            $body[Fields::F_PASSWORD] = $context->get(Fields::F_PASSWORD);
        }

        return [Fields::F_OPERATION => [Fields::F_CUSTOMER => $body,],];
    }
}