<?php

namespace ADW\MindboxV3Bundle\Operation\Customer;

use ADW\MindboxV3Bundle\Client\ClientInterface;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext as Fields;
use ADW\MindboxV3Bundle\Operation\Lib\ConfigBag as Config;
use ADW\MindboxV3Bundle\Operation\Lib\OperationAbstract;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContext as Context;

class LoadCustomerOperation extends OperationAbstract
{
    const OPERATION = 'GetCustomerProfile';
    protected function configure(Config $config, Context $context)
    {
        $config->set(Config::F_OPERATION, static::OPERATION);
        $config->set(Config::F_METHOD, ClientInterface::HTTP_METHOD_POST);
        $config->set(Config::F_BODY, $this->body($context, $config));
        $config->set(Config::F_CACHE_TTL, 10);
    }

    private function body(Context $context, Config $config): array
    {
        $body = [];
        if($context->is(Fields::F_ID)) {
            $body[Fields::F_ID] = $context->get(Fields::F_ID);
        } else if ($config->is(Config::F_AUTH_ID)){
            $body[Fields::F_ID] = $config->getLink(Config::F_AUTH_ID);
        }

        return [Fields::F_OPERATION => [Fields::F_CUSTOMER => [Fields::F_IDS => $body],],];
    }
}