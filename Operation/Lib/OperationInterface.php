<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;


use ADW\MindboxV3Bundle\Operation\Lib\Processor\ProcessorInterface;

interface OperationInterface
{
    public function tune();
    public function prepare();
    public function run();
    public function getProcessor($config) :ProcessorInterface;
    public function getContainer() :OperationContainer;
}