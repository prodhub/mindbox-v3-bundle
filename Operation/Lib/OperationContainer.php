<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;

use ADW\MindboxV3Bundle\Helper\ModularContainer;

class OperationContainer extends ModularContainer
{
    /** Errors for user */
    public $visibleErrors;
    /** Internal hidden errors */
    public $errors = [];
    /** Mindbox internal error_no */
    public $errorId;
    public $httpStatusCode;
    public $isStatusSuccess = false;
    public $time = 0;
}