<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;

use ADW\MindboxV3Bundle\Operation\Lib\ConfigBag as Config;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContext as Context;
use ADW\MindboxV3Bundle\Operation\Lib\Processor\MindboxV3Processor;
use ADW\MindboxV3Bundle\Operation\Lib\Processor\ProcessorInterface;
use ADW\MindboxV3Bundle\Operation\Lib\Processor\ReRunProcessorInterface;

abstract class OperationAbstract implements OperationInterface, ContainerableInterface
{
    /** @var Context */
    protected $context;
    /** @var OperationContainer */
    protected $container;
    /** @var ConfigBag */
    protected $config;
    /** @var ProcessorInterface */
    protected $processor;

    public function __construct(Context $context = null, OperationContainer $container = null)
    {
        if(!$container) {
            $container = new OperationContainer();
        }
        if(!$context) {
            $context = new OperationContext();
        }
        $this->context   = $context;
        $this->container = $container;
    }

    abstract protected function configure(Config $config, Context $context);

    /*
     * Use this function for configuring request
     */
    public function tune()
    {
        $context    = $this->context;
        $config     = new ConfigBag();
        $deviceGuid = null;

        if(!$config->is(Config::F_AUTH_ID)) {
            $userId = null;
            if(\is_callable($fn = $context->getEnvLink(Context::E_FN_USER_ID))) {
                $userId = $fn();
            }
            if($userId > 0) {
                $config->set(Config::F_AUTH_ID, $userId);
            }
        }

        $this->configure($config, $context);

        if(!$config->has(Config::F_TIMEOUT)) {
            $config->set(Config::F_TIMEOUT, Config::DEFAULT_TIMEOUT);
        }

        if(!$config->has(Config::F_IS_NEED_AUTH)) {
            $value = Config::DEFAULT_IS_NEED_AUTH;
            if(null !== $contextValue = $context->get(Context::F_IS_NEED_AUTH, null)) {
                $value = $contextValue;
            }
            $config->set(Config::F_IS_NEED_AUTH, $value);
        }

        if(!$config->has(Config::F_METHOD)) {
            $config->set(Config::F_METHOD, Config::DEFAULT_METHOD);
        }

        if(!$config->has(Config::F_EXCEPT_ANSWER)) {
            $value = Config::DEFAULT_EXCEPT_ANSWER;
            if(null !== $contextValue = $context->get(Context::F_EXCEPT_ANSWER, null)) {
                $value = $contextValue;
            }
            $config->set(Config::F_EXCEPT_ANSWER, $value);
        }

        if(!$config->has(Config::F_URL) && $context->is(Context::F_URL)) {
            $config->set(Config::F_URL, $context->get(Context::F_URL));
        }

        if(!$config->has(Config::F_SECRET)) {
            $config->set(Config::F_SECRET, $context->getEnv(Context::E_SECRET));
        }

        if(!$config->has(Config::F_CACHE)) {
            if($context->is(Context::F_FORCE_CACHE)) {
                $config->set(Config::F_CACHE, $context->get(Context::F_FORCE_CACHE));
            } else if($config->is(Config::F_CACHE_TTL)) {
                $config->set(Config::F_CACHE, $config->get(Config::F_CACHE_TTL));
            }
        } else {
            throw new \InvalidArgumentException('For enable cache, use $config->set(Config::F_CACHE_TTL) in $this->configure()');
        }

        $config->set(Config::F_DEVICE_GUID, $context->getEnv(Context::E_DEVICE_GUID));
        $config->set(Config::F_CLIENT_IP, $context->getEnv(Context::E_CLIENT_IP));
        $config->set(Config::F_ENDPOINT, $context->getEnv(Context::E_ENDPOINT_ID));
        $config->setEnv(Config::E_DISPATCHER, $context->getEnv(Context::E_EVENT_DISPATCHER));
        $config->setEnv(Config::E_CLIENT, $context->getEnv($config->is(Config::F_EXCEPT_ANSWER) ? Context::E_CLIENT
            : Context::E_CLIENT_ASYNC));

        $this->config = $config;
        if(!$this->processor) {
            $this->processor = $this->getProcessor($config);
        }
    }

    /**
     * Use this function for multithreading requests. Send request here, receive in run()
     */
    public function prepare()
    {
        $this->processor->request();
    }

    /*
     * Read response (with re-auth) and put results in internal container
     */
    public function run()
    {
        $this->processor->response();
        if($this->processor instanceof ReRunProcessorInterface && $this->processor->isNeedReRun()) {
            $this->tune();
            $this->prepare();
            $this->run();
        }
        $this->processor->transform($this->container);
    }

    public function getContainer() :OperationContainer
    {
        return $this->container;
    }

    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param Config $config
     * @return ProcessorInterface
     */
    public function getProcessor($config): ProcessorInterface
    {
        return new MindboxV3Processor($config);
    }
}