<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;

interface ContainerableInterface
{
    public function getContainer();
    public function getContext();
}