<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;


use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Helper\ModularContext;

class ConfigBag extends ModularContext
{
    /** <Constants> */
    const FORMAT_XML  = 'XML';
    const FORMAT_JSON = 'JSON';
    /** </Constants> */

    /** <Default values> */
    const DEFAULT_METHOD        = MindboxRequest::DEFAULT_METHOD;
    const DEFAULT_EXCEPT_ANSWER = true;
    const DEFAULT_IS_NEED_AUTH  = true;
    const DEFAULT_TIMEOUT       = 10;
    /** </Default> */

    const E_CLIENT     = 'client';
    const E_DISPATCHER = 'eventDispatcher';

    const F_SECRET        = 'secret'; # %adw_mindbox.credentials.secret_key%
    const F_CLIENT_IP     = 'clientIP';
    const F_DEVICE_GUID   = 'deviceGUID';
    const F_EXCEPT_ANSWER = 'isExceptAnswer'; # run request in this runtime (true = use MindboxClient::class, false = use MindboxAsyncClient::class)
    const F_IS_NEED_AUTH  = 'isNeedAuth'; # set-up cookie with device UUID and header with client IP
    const F_METHOD        = 'method';
    const F_BODY          = 'body';
    const F_HEADERS       = 'headers';
    const F_CACHE_TTL     = 'cacheTtl'; # cache time (0 for disable cache), may override by OperationContext::F_FORCE_CACHE
    const F_AUTH_ID       = 'authId';
    const F_OPERATION     = 'operation'; # operation's name in request URL
    const F_URL           = 'customURL'; # override url of request (not recommended)
    const F_CACHE         = 'cacheTime'; # use self::F_CACHE_TTL for setup a cache time
    const F_ENDPOINT      = 'endPoint'; # %adw_mindbox.credentials.end_point%
    const F_TIMEOUT       = 'timeout'; # guzzle connection_timeout
}