<?php

namespace ADW\MindboxV3Bundle\Operation\Lib\Processor;

interface ReRunProcessorInterface
{
    // If you're need to make retry,
    // implement this interface in processor,
    // release re-auth via a event dispatcher,
    // and lock recursive call by internal variable.
    public function isNeedReRun();
}