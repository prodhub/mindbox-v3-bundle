<?php

namespace ADW\MindboxV3Bundle\Operation\Lib\Processor;

use ADW\MindboxV3Bundle\Client\ClientInterface;
use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Client\Lib\MindboxResponse;
use ADW\MindboxV3Bundle\Client\MindboxResponseFields as MBFields;
use ADW\MindboxV3Bundle\Helper\XmlDomConstruct;
use ADW\MindboxV3Bundle\Operation\Lib\ConfigBag as Config;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContainer;
use function GuzzleHttp\Psr7\mimetype_from_extension;

class MindboxV3Processor implements ProcessorInterface
{
    const QUERY_OPERATION   = 'operation';
    const QUERY_DEVICE_UUID = 'DeviceUUID';
    const QUERY_ENDPOINT_ID = 'endpointId';
    const DEFAULT_FORMAT    = Config::FORMAT_XML;
    const HEADER_CACHE_TTL  = ClientInterface::HEADER_CACHE_TTL;

    protected $config;
    protected $cachedRequests;
    protected $cachedResponses;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function request()
    {
        $config = $this->config;
        $client = $config->getEnv(Config::E_CLIENT);
        if(!$client instanceof ClientInterface) {
            throw new \LogicException('Configuration must have client interface in environment');
        }

        $client->request($this->getRequestContext($config), $this->getResponseContainer($config));
    }

    public function response()
    {
        $config = $this->config;
        $client = $config->getEnv(Config::E_CLIENT);
        if(!$client instanceof ClientInterface) {
            throw new \LogicException('Configuration must have client interface in environment');
        }

        $client->response($this->getResponseContainer($config));
    }

    public function transform($container)
    {
        if(!$container instanceof OperationContainer) {
            return;
        }
        $config          = $this->config;
        $from            = $this->getResponseContainer($config);
        $status          = null;
        $result          = [];
        $isStatusSuccess = false;
        $errors          = $visibleErrors = [];
        $errorId         = $httpStatusCode = null;
        $data            = null;
        $raw             = $from->body;

        if($raw && \is_string($raw)) {
            try {
                $data = simplexml_load_string($raw, null, LIBXML_NOCDATA);
            } catch(\Exception $e) {
                if($raw{1} !== '<') {
                    //МБ ответил не XML, a plain text c ошибкой, фиксить не будут (06.02.18)
                    $data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><result><errorMessage>' . $raw . '</errorMessage></result>');
                }
            }
        } else if($raw instanceof \SimpleXMLElement) {
            $data = $raw;
        }

        if($data instanceof \SimpleXMLElement) {
            $result          = json_decode(json_encode($data), true);
            $status          = $result[MBFields::F_STATUS] ?? null;
            $isStatusSuccess = $data->getName() === MBFields::F_RESULT && strtolower($status) === strtolower(MBFields::S_SUCCESS);
        }

        if(!empty($result) && \is_array($result)) {
            foreach($result as $k => $v) {
                if(!empty($v['@attributes']['nil'])) {
                    $result[$k] = null;
                }
            }
        }

        switch(strtolower($status)) {
            case strtolower(MBFields::S_V_ERROR):
                if(!empty($result[MBFields::F_V_MESSAGES]) && \is_array($result[MBFields::F_V_MESSAGES])) {
                    foreach($result[MBFields::F_V_MESSAGES] as $vm) {
                        isset($vm[MBFields::F_MESSAGE]) && $visibleErrors[] = $vm[MBFields::F_MESSAGE];
                    }
                }
                break;
            case strtolower(MBFields::S_I_ERROR):
            case strtolower(MBFields::S_P_ERROR):
                if(!empty($result[MBFields::F_ERROR_MESSAGE])) {
                    $errors[] = $result[MBFields::F_ERROR_MESSAGE];
                }
                if(!empty($result[MBFields::F_ERROR_ID])) {
                    $errorId = $result[MBFields::F_ERROR_ID];
                }
                if(!empty($result[MBFields::F_HTTP_CODE])) {
                    $httpStatusCode = $result[MBFields::F_HTTP_CODE];
                }
                break;
            case strtolower(MBFields::S_T_ERROR):
                //@todo: what is it? Maybe async method?
                break;
        }

        $container->time            = $from->requestTime;
        $container->status          = &$status;
        $container->isStatusSuccess = $isStatusSuccess;
        $container->result          = &$result;
        $container->errors          = &$errors;
        $container->errorId         = $errorId;
        $container->httpStatusCode  = $httpStatusCode ?? $from->statusCode;
        $container->visibleErrors   = &$visibleErrors;
    }

    public function getRequestContext(Config $config)
    {
        if(!isset($this->cachedRequests[$version = $config->version()])) {
            $request              = new MindboxRequest([
                MindboxRequest::F_USER_ID => $config->get(Config::F_AUTH_ID),
                MindboxRequest::F_URI     => $this->getUrl($config),
                MindboxRequest::F_METHOD  => $config->get(Config::F_METHOD),
                MindboxRequest::F_HEADERS => $this->getHeaders($config),
                MindboxRequest::F_BODY    => $this->getBody($config), /** array */
                MindboxRequest::F_TIMEOUT => $config->get(Config::F_TIMEOUT),
            ]);
            $this->cachedRequests = [$version => $request];
        }

        return $this->cachedRequests[$version];
    }

    public function getResponseContainer(Config $config): MindboxResponse
    {
        if(!isset($this->cachedResponses[$version = $config->version()])) {
            $response              = new MindboxResponse();
            $response->format      = $this->getFormat();
            $this->cachedResponses = [$version => $response];
        }

        return $this->cachedResponses[$version];
    }

    protected function getUrl(Config $config): string
    {
        if($config->is(Config::F_URL)) {
            return $config->get(Config::F_URL);
        }

        if(!$config->is(Config::F_OPERATION)) {
            throw new \LogicException('Operation\'s name must be set in $config->set(Config::F_OPERATION)');
        }

        $pattern = MindboxRequest::DEFAULT_V3_SYNC_OPERATIONS_URL; //@todo: maybe to use a const MindboxRequest::DEFAULT_V3_ASYNC_OPERATIONS_URL for do not excepted answers?;
        $params  = [
            self::QUERY_OPERATION   => $config->get(Config::F_OPERATION),
            self::QUERY_ENDPOINT_ID => $config->get(Config::F_ENDPOINT),
        ];

        if($config->is(Config::F_IS_NEED_AUTH)) {
            $params[self::QUERY_DEVICE_UUID] = $config->get(Config::F_DEVICE_GUID);
        }

        return $pattern . '?' . \http_build_query($params);
    }

    protected function getHeaders(Config $config)
    {
        if(!$secret = $config->get(Config::F_SECRET)) {
            throw new \LogicException('Do you forget set Mindbox secretKey in services.yml or change default API version? Example, if using for running operations via the service `adw.mindbox.queue.worker`, set calls `setSecret`');
        }

        $headers = $config->get(Config::F_HEADERS, []);
        $mime    = mimetype_from_extension($this->getFormat());
        $headers += [
            'Accept'        => $mime,
            'Content-Type'  => $mime,
            'Authorization' => 'Mindbox secretKey="' . $secret . '"',
        ];

        if($config->is(Config::F_IS_NEED_AUTH)) {
            $headers['X-Customer-IP'] = $config->get(Config::F_CLIENT_IP);
        }

        if($config->is(Config::F_CACHE)) {
            $headers[self::HEADER_CACHE_TTL] = $config->get(Config::F_CACHE);
        }

        return $headers;
    }

    protected function &getBody(Config $config)
    {
        $body = $config->get(Config::F_BODY, []);
        $doc  = new XmlDomConstruct('1.0', 'utf-8');
        $doc->fromMixed($body);
        $string = $doc->saveXML();
        return $string;
    }

    protected function getFormat()
    {
        return self::DEFAULT_FORMAT;
    }
}