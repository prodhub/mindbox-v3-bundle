<?php

namespace ADW\MindboxV3Bundle\Operation\Lib\Processor;

use ADW\MindboxV3Bundle\Operation\Lib\ConfigBag as Config;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContainer;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContext;

interface ProcessorInterface
{
    public function __construct(Config $config);
    public function request();
    public function response();
    public function transform($container);
}