<?php

namespace ADW\MindboxV3Bundle\Operation\Lib;

use ADW\MindboxV3Bundle\Helper\ModularContext;

/**
 * Context which place into request with custom request parameters.
 * Protection against the use of global variables.
 *
 * Class OperationContext
 * @package ADW\MindboxV3Bundle\Operation\Lib
 */
class OperationContext extends ModularContext
{

    /** <Field of operations' configuration context, which moved outside of operation's runtime> */
    const F_EXCEPT_ANSWER = 'exceptAnswer'; # Are you wait answer for this operation?
    const F_FORCE_CACHE   = 'isForceCache'; # force caching answer from mindbox (if needle cache async)
    const F_URL           = 'url'; # override request url, not recommended
    const F_IS_NEED_AUTH  = 'isNeedAuth'; # read id from token storage, set device guid and customer IP (if using V3Processor)
    /** </Field> */

    /** <Field of operations' environment configuration context> */
    const E_CLIENT           = 'client';
    const E_CLIENT_ASYNC     = 'clientAsync';
    const E_ENDPOINT_ID      = 'endPointId';
    const E_SECRET           = 'mindBoxAuthSecretKey';
    const E_CLIENT_IP        = 'clientIP';
    const E_EVENT_DISPATCHER = 'eventDispatcher';
    const E_DEVICE_GUID      = 'deviceGUID';
    const E_FN_USER_ID       = 'userId';
    /** </Field> */
}