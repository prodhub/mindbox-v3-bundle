<?php

namespace ADW\MindboxV3Bundle\Command;

use ADW\MindboxV3Bundle\Client\Lib\MindboxRequest;
use ADW\MindboxV3Bundle\Client\Lib\MindboxResponse;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MindboxCallCommand extends ContainerAwareCommand
{
    /** @var LoggerInterface */
    protected $logger;
    /** @var OutputInterface */
    protected $output;

    protected function configure()
    {
        $this
            ->setName('adw:mindbox:call')
            ->setDescription('Асинхронный вызов Mindbox в отдельном процессе')
            ->addArgument('context', null, 'Сериализованный контекст отправляемого параметра (MindboxRequest инстанс)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $serviceContainer = $this->getContainer();
        $this->logger     = $serviceContainer->get('monolog.logger.adw.mindbox');
        $message          = null;

        if(!$context = $input->getArgument('context')) {
            $this->log(Logger::CRITICAL, 'Не указан параметр "context"');
            exit;
        }

        try {
            $context = @\unserialize(\base64_decode($context), ['allowed_classes' => [MindboxRequest::class]]);
        } catch(\Throwable $e) {
            $message = $e->getMessage();
        }

        if(!$context instanceof MindboxRequest) {
            $this->log(Logger::CRITICAL, sprintf('Unable to unpack a context `%s`%s', $context, $message
                ? ' with message: ' . $message : '.'));
            exit;
        }

        $client    = $serviceContainer->get('adw.mindbox.client');
        $container = new MindboxResponse();
        $client->request($context, $container);
        $client->response($container);
    }

    protected function log($level, $message)
    {
        $this->logger->log($level, $message);
        $this->output->writeln($message);
    }
}