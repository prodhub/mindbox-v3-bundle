<?php

namespace ADW\MindboxV3Bundle\Controller;

use ADW\MindboxV3Bundle\Entity\Customer;
use ADW\MindboxV3Bundle\Operation\Customer\CustomerPasswordAuthOperation;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext;
use ADW\MindboxV3Bundle\Operation\Lib\OperationContainer;
use ADW\MindboxV3Bundle\Security\Token\ExternalCustomerToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/mindbox")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $customer = null;
        /** @var ExternalCustomerToken $token */
        $token = $this->get('security.token_storage')->getToken();
        if($token instanceof  ExternalCustomerToken) {
            $customer = $token->getCustomer();
            $id       = $token->getId();
        }


        $user = $this->get('adw.mindbox.customer.repository')->find(28);
//        $context = new CustomerOperationContext([
//            CustomerOperationContext::F_EMAIL         => 'anton.lezhepekov@mail.ru',
//            CustomerOperationContext::F_PASSWORD      => 'Nameless300',
//            CustomerOperationContext::F_EXCEPT_ANSWER => true,
//        ]);
//        $container = new OperationContainer();
//        $operation = new CustomerPasswordAuthOperation($context, $container);
//        $this->get('adw.mindbox.queue.worker')->run_operation($operation);

        $debug = 21;
        // replace this example code with whatever you need
        return $this->render('@ADWMindboxV3/debug.html.twig', [
            'debug' => $debug,
        ]);
    }
}