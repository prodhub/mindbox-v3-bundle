<?php

namespace ADW\MindboxV3Bundle\Repository;

use ADW\MindboxV3Bundle\Entity\CustomerInterface;
use ADW\MindboxV3Bundle\Operation\Customer\Lib\CustomerOperationContext as Context;
use ADW\MindboxV3Bundle\Security\Token\CustomerTokenInterface;
use ADW\MindboxV3Bundle\Service\MindboxCustomerService;

class CustomerRepository
{
    protected $_contextClass;
    /**
     * @var MindboxCustomerService
     */
    protected $_em;
    protected $_entityName;
    protected $_entityInst;

    public function __construct(MindboxCustomerService $em, $entityClass)
    {
        $this->_em = $em;
        $this->setCustomerClass($entityClass);
    }

    public function auth(CustomerTokenInterface $token) :CustomerInterface
    {
        return $this->_em->auth($this->_entityName, $token);
    }

    public function find($id)
    {
        /** @var Context $context */
        $context = new Context;
        return $this->_em->find($this->_entityName, [
            $context::F_ID => $id,
        ]);
    }

    /**
     * @return array|void
     * @throws \RuntimeException
     */
    public function findAll()
    {
        throw new \RuntimeException(sprintf('Method `%s` is disabled', __METHOD__));
    }

    public function findBy(array $criteria)
    {
        return $this->_em->find($this->_entityName, $criteria);
    }

    public function findOneBy(array $criteria)
    {
        return $this->findBy($criteria);
    }

    public function persist($object)
    {
        if(!$object instanceof $this->_entityInst) {
            throw new \LogicException(sprintf('Service `%s` can save only entity with instance of `%s`', basename(__CLASS__), basename($this->_entityName)));
        }

        $this->_em->persist($object);
    }

    public function flush()
    {
        $this->_em->flush();
    }

    public function setContextClass($className)
    {
        $class = null;
        if(\class_exists($className)) {
            $class = new $className;
        }
        if(!$class || !$class instanceof Context) {
            throw new \InvalidArgumentException(sprintf('Context class must be instance of OperationContext::class. Check call `setContextClass` in service.yml for service `%s`', __CLASS__));
        }

        $this->_contextClass = $className;
    }

    public function setCustomerClass($className)
    {
        $class = null;
        if(\class_exists($className)) {
            $class = new $className;
        }
        if(!$class || !$class instanceof CustomerInterface) {
            throw new \InvalidArgumentException(sprintf('Customer class must be instance of CustomerInterface::class. Check call `setCustomerClass` in service.yml for service `%s`', __CLASS__));
        }

        $this->_entityName = $className;
        $this->_entityInst = $class;
    }
}